//
//  ViewController.m
//  alarm
//
//  Created by Prince on 11/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"
#define kRefreshTimeInSeconds 1
@interface ViewController (){
    int count;
}
@property (strong, nonatomic) IBOutlet UIDatePicker *datepicker1;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *cancleButton;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UIImageView *imageView2;
@end

@implementation ViewController
@synthesize datepicker1;
@synthesize label1;
@synthesize imageView2;
- (void)viewDidLoad {
    
    
    datepicker1.hidden=FALSE;
    
    datepicker1.datePickerMode = UIDatePickerModeTime;
    datepicker1.minuteInterval=1;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)saveButtonPressed:(id)sender {
    
   NSDate *currentTime = [[NSDate alloc] init];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //dateFormatter.timeZone=[NSTimeZone defaultTimeZone];
    dateFormatter.timeStyle=NSDateFormatterShortStyle;          //gives the time formet like 10:34:20
    dateFormatter.dateStyle=NSDateFormatterShortStyle;         //gives the date like 10/2/15.

    
    NSString *dateTimeString=[dateFormatter stringFromDate:datepicker1.date];
    NSLog(@"button save pressed :%@",dateTimeString);
    NSTimeInterval diff = [datepicker1.date timeIntervalSinceDate:currentTime];   //used to find the time gap between source time to destination time.
    
    
   label1.text = [NSString stringWithFormat:@"%0.f",diff];
    
    datepicker1.hidden=TRUE;
    
    NSTimer *timer=[[NSTimer alloc]init];
   
    timer=(NSTimer*)[NSString stringWithFormat:@"%f",diff];
   
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(targetMethod) userInfo:nil repeats:NO];
    [timer invalidate];
    timer =nil;
    label1.text=[NSString stringWithFormat:@"%@",timer];
}


-(void)targetMethod{
    
}


- (IBAction)cancleButtonPressed:(id)sender
{
  label1.text=@"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
